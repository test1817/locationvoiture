-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 09 nov. 2020 à 12:11
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion_location_voitures`
--

-- --------------------------------------------------------

--
-- Structure de la table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(50) DEFAULT NULL,
  `id_ville` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_ville` (`id_ville`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `addresses`
--

INSERT INTO `addresses` (`id`, `address`, `id_ville`, `created_at`, `updated_at`) VALUES
(1, 'test1', 1, NULL, NULL),
(2, 'lorenza', 1, '2020-09-02 15:35:59', '2020-09-02 15:35:59');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `pieceIdentite` varchar(30) NOT NULL,
  `numPiece` varchar(30) NOT NULL,
  `dateDelivrPiece` varchar(10) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `numPermis` varchar(30) NOT NULL,
  `dateDelivrPermis` varchar(10) NOT NULL,
  `lieuDelivrPiece` varchar(40) NOT NULL,
  `lieuDelivrPermis` varchar(40) NOT NULL,
  `dateNaissance` varchar(10) NOT NULL,
  `lieuNaissance` varchar(40) NOT NULL,
  `codePostale` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pieceIdentiteScan` text DEFAULT NULL,
  `permisScan` text DEFAULT NULL,
  `nationalite` varchar(40) NOT NULL,
  `addresse` varchar(300) NOT NULL,
  `ville` varchar(40) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `dateLivrePermis` varchar(10) NOT NULL,
  `dateLivrePiece` varchar(10) NOT NULL,
  PRIMARY KEY (`numPiece`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`pieceIdentite`, `numPiece`, `dateDelivrPiece`, `nom`, `prenom`, `telephone`, `numPermis`, `dateDelivrPermis`, `lieuDelivrPiece`, `lieuDelivrPermis`, `dateNaissance`, `lieuNaissance`, `codePostale`, `email`, `pieceIdentiteScan`, `permisScan`, `nationalite`, `addresse`, `ville`, `created_at`, `updated_at`, `dateLivrePermis`, `dateLivrePiece`) VALUES
('cin', '111', '2020-09-03', 'anas', 'hamid', '0610', '14', '2020-09-08', '1', '1', '2020-09-09', '1', '4600', 'email@gmail.com', '111_2020-09-11-18-20-03_ajaxdata_0.png', 'walo', '2', 'adrrr1', '1', '2020-09-09 09:49:16', '2020-09-11 18:20:03', '', ''),
('PASSPORT', '3333', '2020-07-31', 'hghg', 'hhghg', '222222', '3333', '2020-07-03', '1', '1', '2020-07-24', '1', '33333', 'jbxsxjbx@gmail.com', './src/PackMenuForm/MesPanel/Scans/P_ID_ReNoteS5S6.jpg', './src/PackMenuForm/MesPanel/Scans/Permis_line.JPG', '2', 'adr1', '1', NULL, NULL, '', ''),
('cin', 'eee', '2020-09-01', 'il2', 'pre2', '0610', '14', '2020-09-02', '1', '1', '2020-09-03', '1', '4600', 'email@gmail.com', 'eee_2020-09-11-14-32-16_ajax.png', 'eee_2020-09-11-14-32-16_ajax.png', '1', 'adrrr1', '1', '2020-09-11 14:32:16', '2020-09-11 14:32:16', '', ''),
('CIN', 'eee1', '2020-09-01', 'test', 'pre', '0610', '14', '2020-09-02', '1', '1', '2020-09-03', '1', '4600', 'email@gmail.com', 'walo', 'eee1_2020-09-11-14-25-02_ajax.png', '1', 'adrrr1', '1', '2020-09-11 14:25:02', '2020-10-04 17:25:02', '', ''),
('CIN', 'hh2233', '2020-07-31', 'SKIRI', 'kenza', '0909090909', '788787', '2020-07-31', '1', '1', '2000-11-04', '1', '87888', 'skirikanza@gmail.com', './src/PackMenuForm/MesPanel/Scans/P_ID_P_ID_020___1585075585.jpg', './src/PackMenuForm/MesPanel/Scans/Permis_Permis_laboratory.JPG', '2', 'adr2', '1', NULL, NULL, '', ''),
('CIN', 'hh8877', '2020-07-31', 'miri', 'miri', '999999', '887', '2020-07-31', '1', '1', '2020-07-04', '1', '9998', 'hgg@gmail.com', './src/PackMenuForm/MesPanel/Scans/P_ID_914159.png', 'null', '1', 'adr3', '1', NULL, NULL, '', ''),
('CIN', 'hhkk11', '2020-09-01', 'bahajou', 'yassine', '0606060606', '889988', '2022-08-19', '1', '1', '1995-12-11', '1', '46000', 'yassine@gmail.com', './src/PackMenuForm/MesPanel/Scans/P_ID_type.JPG', './src/PackMenuForm/MesPanel/Scans/Permis_medicament.JPG', '2', 'adr4', '2', NULL, NULL, '', ''),
('pppttt', 'pppp', '2020-09-01', 'nnn', 'pppp', '0666', 'nnnnn', '2020-09-02', '1', '1', '2020-09-03', '1', 'hhhhhh', 'hhhh@gmail.com', 'hhh', 'hhh', '1', 'adr5', '3', '2020-09-04 18:58:02', '2020-09-04 18:58:02', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coleur` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `colors`
--

INSERT INTO `colors` (`id`, `coleur`) VALUES
(1, 'Noir'),
(2, 'Blanc');

-- --------------------------------------------------------

--
-- Structure de la table `curburants`
--

DROP TABLE IF EXISTS `curburants`;
CREATE TABLE IF NOT EXISTS `curburants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curburant` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `curburants`
--

INSERT INTO `curburants` (`id`, `curburant`) VALUES
(1, 'Gasoil'),
(2, 'Essence');

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `payes`
--

DROP TABLE IF EXISTS `payes`;
CREATE TABLE IF NOT EXISTS `payes` (
  `id` int(11) NOT NULL,
  `paye` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `payes`
--

INSERT INTO `payes` (`id`, `paye`) VALUES
(1, 'Maroc'),
(2, 'Algerie'),
(3, 'France');

-- --------------------------------------------------------

--
-- Structure de la table `prolongations`
--

DROP TABLE IF EXISTS `prolongations`;
CREATE TABLE IF NOT EXISTS `prolongations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_res` varchar(30) NOT NULL,
  `date_debut` varchar(10) NOT NULL,
  `date_fin` varchar(10) NOT NULL,
  `avance` varchar(10) NOT NULL,
  `mode_paiement` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `prolongations`
--

INSERT INTO `prolongations` (`id`, `num_res`, `date_debut`, `date_fin`, `avance`, `mode_paiement`, `created_at`, `updated_at`) VALUES
(3, '1', '2020-10-10', '2020-10-23', '100', 'espece', '2020-10-08 14:23:35', '2020-10-08 14:23:35');

-- --------------------------------------------------------

--
-- Structure de la table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE IF NOT EXISTS `reservations` (
  `numRes` varchar(30) NOT NULL,
  `numPiece` varchar(30) NOT NULL,
  `immatricule` varchar(30) NOT NULL,
  `Date_depart` varchar(50) NOT NULL,
  `Lieu_depart` varchar(30) NOT NULL,
  `Date_retour` varchar(50) NOT NULL,
  `Date_retour_reelle` varchar(30) DEFAULT NULL,
  `Lieu_retour` varchar(30) NOT NULL,
  `pu` double NOT NULL,
  `Montant` varchar(30) NOT NULL,
  `Mode_paiement` varchar(30) NOT NULL,
  `numPieceConducteur` varchar(30) DEFAULT NULL,
  `nom_banq` varchar(30) DEFAULT NULL,
  `num_cart` varchar(30) DEFAULT NULL,
  `depot` float NOT NULL,
  `solde` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `carburant_depart` varchar(30) DEFAULT NULL,
  `carburant_retour` varchar(30) DEFAULT NULL,
  `km_depart` varchar(10) DEFAULT NULL,
  `km_retour` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`numRes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reservations`
--

INSERT INTO `reservations` (`numRes`, `numPiece`, `immatricule`, `Date_depart`, `Lieu_depart`, `Date_retour`, `Date_retour_reelle`, `Lieu_retour`, `pu`, `Montant`, `Mode_paiement`, `numPieceConducteur`, `nom_banq`, `num_cart`, `depot`, `solde`, `created_at`, `updated_at`, `carburant_depart`, `carburant_retour`, `km_depart`, `km_retour`) VALUES
('1', '111', '1440', '2020-10-01', 'ss', '2020-10-10', NULL, 'sss', 100, '900', 'espece', NULL, NULL, NULL, 900, 0, '2020-10-07 18:13:02', '2020-10-07 18:13:02', '20', '40', '100', '300');

-- --------------------------------------------------------

--
-- Structure de la table `typevehicules`
--

DROP TABLE IF EXISTS `typevehicules`;
CREATE TABLE IF NOT EXISTS `typevehicules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ModeleV` varchar(30) NOT NULL,
  `MarqueV` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `typevehicules`
--

INSERT INTO `typevehicules` (`id`, `ModeleV`, `MarqueV`) VALUES
(1, 'A1', 'Aston Martin'),
(2, 'DB11', 'Aston Martin'),
(3, 'DB9', 'Aston Martin'),
(4, 'DBS', 'Aston Martin'),
(5, 'Giulia', 'Alfa Romeo'),
(6, 'Giulietta', 'Alfa Romeo'),
(7, 'Mito', 'Alfa Romeo'),
(8, 'RAPIDE', 'Aston Martin'),
(9, 'V8 Vantage', 'Aston Martin'),
(10, 'VANQUISH', 'Aston Martin');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$UtPN/abJkfw.ltEfiaW0LeUXBsV1DvfNphveH51heCvTzi20V/cTi', NULL, '2020-10-09 14:46:31', '2020-10-09 14:46:31');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `Nom` varchar(30) NOT NULL,
  `Passe` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`Nom`, `Passe`) VALUES
('bahajou', '0000');

-- --------------------------------------------------------

--
-- Structure de la table `vehicules`
--

DROP TABLE IF EXISTS `vehicules`;
CREATE TABLE IF NOT EXISTS `vehicules` (
  `immatricule` varchar(30) NOT NULL,
  `marque` varchar(30) NOT NULL,
  `modele` varchar(30) NOT NULL,
  `Nombre_Place` varchar(30) NOT NULL,
  `Puissance` varchar(30) NOT NULL,
  `Date_circulation` varchar(10) NOT NULL,
  `Lieu_stationnement` varchar(30) NOT NULL,
  `Num_chassis` varchar(30) NOT NULL,
  `Code_radio` varchar(30) NOT NULL,
  `Categorie` varchar(30) NOT NULL,
  `Curburant` varchar(30) NOT NULL,
  `Couleur` varchar(30) NOT NULL,
  `Delai_debut` varchar(10) NOT NULL,
  `Delai_fin` varchar(10) NOT NULL,
  `disponible` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`immatricule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vehicules`
--

INSERT INTO `vehicules` (`immatricule`, `marque`, `modele`, `Nombre_Place`, `Puissance`, `Date_circulation`, `Lieu_stationnement`, `Num_chassis`, `Code_radio`, `Categorie`, `Curburant`, `Couleur`, `Delai_debut`, `Delai_fin`, `disponible`, `created_at`, `updated_at`) VALUES
('1440', 'clio', '2', '2', '80', '2020/07/24', 'lieuss', 'sss', 'sss', 'cccc', 'ccc', 'ccc', '2020-10-01', '2020-10-31', 0, '2020-10-04 18:05:15', '2020-10-06 12:29:45'),
('2014 RT 44', 'Aston Martin', 'A1', '3', '6666', '2020/07/24', 'Agence B', '3355', '4455', 'Tourisme', 'Gasoil', 'Abricot', '2020/07/24', '2020/07/24', 1, NULL, NULL),
('2014 RT 56', 'Aston Martin', 'DB9', '4', '4444', '2020/07/24', 'Agence A', '3333', '4444', 'Tourisme', 'Gasoil', 'Abricot', '2020/07/24', '2020/07/24', 1, NULL, NULL),
('hghgh', 'Aston Martin', 'DB11', '7', '88', '2020/07/24', 'Agence A', '989898', '8989', 'Monospace', 'Essence', 'Abricot', '2020/07/31', '2020/07/31', 1, NULL, NULL),
('mm1122', 'Alfa Romeo', 'Mito', '4', '44', '2020/08/29', 'Agence A', '22', '2222', 'Tourisme', 'Essence', 'Abricot', '2020/08/01', '2020/08/31', 1, NULL, NULL),
('test', 'Alfa Romeo', 'Mito', 'ggg', 'gggg', '2020/07/24', 'gggg', 'qqq', 'qqq', 'qqqq', 'qqqq', 'qqqq', '2020/07/24', '2020/07/24', 1, '2020-09-13 20:07:20', '2020-09-13 20:07:20');

-- --------------------------------------------------------

--
-- Structure de la table `villes`
--

DROP TABLE IF EXISTS `villes`;
CREATE TABLE IF NOT EXISTS `villes` (
  `id` int(11) NOT NULL,
  `ville` varchar(30) DEFAULT NULL,
  `id_paye` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PayeVille` (`id_paye`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `villes`
--

INSERT INTO `villes` (`id`, `ville`, `id_paye`) VALUES
(1, 'Safi', 1),
(2, 'Casa Blanca', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
